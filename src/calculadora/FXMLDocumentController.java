/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package calculadora;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
/**
 *
 * @author augus
 */
public class FXMLDocumentController implements Initializable {
   @FXML
   private Label resultCel;
   @FXML
   private Label resultKelvin;
   @FXML
   private Label resultFah;
   @FXML
   private TextField leitor;
   @FXML
   private Button botaoCelsius;
   @FXML
   private Button botaoKelvin;
   @FXML
   private Button botaoFahrenheit;
   @FXML
   private void fahrenheit(){
   double i = Double.parseDouble(leitor.getText());
   resultFah.setText(""+i);
   double tf = (i-32)/1.8;
   resultCel.setText(""+tf);
   tf=((i-32)*(5.0000/9.0000))+273.15;
   resultKelvin.setText(""+tf);
   }
   @FXML
   private void celsius(){
   double i = Double.parseDouble(leitor.getText());
   resultCel.setText(""+i);
   double tf =i+273.15;
   resultKelvin.setText(""+tf);
   tf =(i*1.8)+32;
   resultFah.setText(""+tf);
   }
   @FXML
   private void kelvin(){
   double i = Double.parseDouble(leitor.getText());
   resultKelvin.setText(""+i);
   double tf =i-273.15;
   resultCel.setText(""+tf);
   tf= (i-273.15)*1.8000+32;
   resultFah.setText(""+tf);
   }
   
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    
}
